<?php

namespace Drupal\carousel_block\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Carousel entity entities.
 *
 * @ingroup carousel_block
 */
class CarouselEntityDeleteForm extends ContentEntityDeleteForm {


}
