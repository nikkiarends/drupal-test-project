<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/vartheme/templates/system/page.html.twig */
class __TwigTemplate_96c4ae2b98b37ec6147b046fb88cbdce4d4e47da9648ce0b4f40aac4f262b91c extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'hero_slider' => [$this, 'block_hero_slider'],
            'main' => [$this, 'block_main'],
            'header' => [$this, 'block_header'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'action_links' => [$this, 'block_action_links'],
            'help' => [$this, 'block_help'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 59, "if" => 62, "block" => 63];
        $filters = ["escape" => 104, "clean_class" => 68, "t" => 81];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['escape', 'clean_class', 't'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 59
        $context["container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", []), "fluid_container", [])) ? ("container-fluid") : ("container"));
        // line 60
        $context["header_container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", []), "header_container", [])) ? ("container-fluid header-margin") : ("container"));
        // line 62
        if (($this->getAttribute(($context["page"] ?? null), "navigation", []) || $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", []))) {
            // line 63
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 101
        echo "
";
        // line 103
        echo "<div class=\"visible-print-block header-print page-header\">
  <div class=\"";
        // line 104
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\">
    <div class=\"row\">
      <div class=\"col-sm-12\">
        ";
        // line 108
        echo "        ";
        if (($context["logo_print"] ?? null)) {
            // line 109
            echo "          <img class=\"logo pull-left visible-print-inline-block\" src=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["logo_print"] ?? null)), "html", null, true);
            echo "\" />
        ";
        }
        // line 111
        echo "      </div>
    </div>
  </div>
</div>
        
";
        // line 117
        if ($this->getAttribute(($context["page"] ?? null), "hero_slider", [])) {
            // line 118
            $this->displayBlock('hero_slider', $context, $blocks);
        }
        // line 124
        echo "
";
        // line 126
        $this->displayBlock('main', $context, $blocks);
        // line 205
        echo "
";
        // line 206
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 207
            echo "  ";
            $this->displayBlock('footer', $context, $blocks);
        }
        // line 215
        echo "
";
    }

    // line 63
    public function block_navbar($context, array $blocks = [])
    {
        // line 64
        echo "    ";
        // line 65
        $context["navbar_classes"] = [0 => "navbar", 1 => (($this->getAttribute($this->getAttribute(        // line 67
($context["theme"] ?? null), "settings", []), "navbar_inverse", [])) ? ("navbar-inverse") : ("navbar-default")), 2 => (($this->getAttribute($this->getAttribute(        // line 68
($context["theme"] ?? null), "settings", []), "navbar_position", [])) ? (("navbar-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", []), "navbar_position", []))))) : (($context["header_container"] ?? null))), 3 =>         // line 69
($context["header_container"] ?? null)];
        // line 72
        echo "    <header";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["navbar_attributes"] ?? null), "addClass", [0 => ($context["navbar_classes"] ?? null)], "method")), "html", null, true);
        echo " id=\"navbar\" role=\"banner\">
      ";
        // line 73
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["header_container"] ?? null)], "method")) {
            // line 74
            echo "        <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
            echo "\">
      ";
        }
        // line 76
        echo "      <div class=\"navbar-header\">
        ";
        // line 77
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
        echo "
        ";
        // line 79
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 80
            echo "          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
            <span class=\"sr-only\">";
            // line 81
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation"));
            echo "</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
        ";
        }
        // line 87
        echo "      </div>

      ";
        // line 90
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 91
            echo "        <div id=\"navbar-collapse\" class=\"navbar-collapse collapse navbar-right\">
          ";
            // line 92
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])), "html", null, true);
            echo "
        </div>
      ";
        }
        // line 95
        echo "      ";
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["header_container"] ?? null)], "method")) {
            // line 96
            echo "        </div>
      ";
        }
        // line 98
        echo "    </header>
  ";
    }

    // line 118
    public function block_hero_slider($context, array $blocks = [])
    {
        // line 119
        echo "  <div class=\"hero_slider\">
    ";
        // line 120
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "hero_slider", [])), "html", null, true);
        echo "
  </div>
";
    }

    // line 126
    public function block_main($context, array $blocks = [])
    {
        // line 127
        echo "  <div role=\"main\" class=\"main-container ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo " js-quickedit-main-content\">
    <div class=\"row\">

      ";
        // line 131
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 132
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 137
            echo "      ";
        }
        // line 138
        echo "
      ";
        // line 140
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 141
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 146
            echo "      ";
        }
        // line 147
        echo "
      ";
        // line 149
        echo "      ";
        // line 150
        $context["content_classes"] = [0 => ((($this->getAttribute(        // line 151
($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 152
($context["page"] ?? null), "sidebar_first", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 153
($context["page"] ?? null), "sidebar_second", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 154
($context["page"] ?? null), "sidebar_first", [])) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-12") : (""))];
        // line 157
        echo "      <section";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method")), "html", null, true);
        echo ">

        ";
        // line 160
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 161
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 164
            echo "        ";
        }
        // line 165
        echo "
        ";
        // line 167
        echo "        ";
        if (($context["breadcrumb"] ?? null)) {
            // line 168
            echo "          ";
            $this->displayBlock('breadcrumb', $context, $blocks);
            // line 171
            echo "        ";
        }
        // line 172
        echo "
        ";
        // line 174
        echo "        ";
        if (($context["action_links"] ?? null)) {
            // line 175
            echo "          ";
            $this->displayBlock('action_links', $context, $blocks);
            // line 178
            echo "        ";
        }
        // line 179
        echo "
        ";
        // line 181
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "help", [])) {
            // line 182
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 185
            echo "        ";
        }
        // line 186
        echo "
        ";
        // line 188
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 192
        echo "      </section>

      ";
        // line 195
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 196
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 201
            echo "      ";
        }
        // line 202
        echo "    </div>
  </div>
";
    }

    // line 132
    public function block_header($context, array $blocks = [])
    {
        // line 133
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 134
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 141
    public function block_sidebar_first($context, array $blocks = [])
    {
        // line 142
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 143
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 161
    public function block_highlighted($context, array $blocks = [])
    {
        // line 162
        echo "            <div class=\"highlighted\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
        echo "</div>
          ";
    }

    // line 168
    public function block_breadcrumb($context, array $blocks = [])
    {
        // line 169
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["breadcrumb"] ?? null)), "html", null, true);
        echo "
          ";
    }

    // line 175
    public function block_action_links($context, array $blocks = [])
    {
        // line 176
        echo "            <ul class=\"action-links\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["action_links"] ?? null)), "html", null, true);
        echo "</ul>
          ";
    }

    // line 182
    public function block_help($context, array $blocks = [])
    {
        // line 183
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
        echo "
          ";
    }

    // line 188
    public function block_content($context, array $blocks = [])
    {
        // line 189
        echo "          <a id=\"main-content\"></a>
          ";
        // line 190
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
        ";
    }

    // line 196
    public function block_sidebar_second($context, array $blocks = [])
    {
        // line 197
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 198
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 207
    public function block_footer($context, array $blocks = [])
    {
        // line 208
        echo "    <div class=\"footer-wrapper\">
      <footer class=\"footer ";
        // line 209
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\" role=\"contentinfo\">
        ";
        // line 210
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
        echo "
      </footer>
    </div>
  ";
    }

    public function getTemplateName()
    {
        return "themes/vartheme/templates/system/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  434 => 210,  430 => 209,  427 => 208,  424 => 207,  417 => 198,  414 => 197,  411 => 196,  405 => 190,  402 => 189,  399 => 188,  392 => 183,  389 => 182,  382 => 176,  379 => 175,  372 => 169,  369 => 168,  362 => 162,  359 => 161,  352 => 143,  349 => 142,  346 => 141,  339 => 134,  336 => 133,  333 => 132,  327 => 202,  324 => 201,  321 => 196,  318 => 195,  314 => 192,  311 => 188,  308 => 186,  305 => 185,  302 => 182,  299 => 181,  296 => 179,  293 => 178,  290 => 175,  287 => 174,  284 => 172,  281 => 171,  278 => 168,  275 => 167,  272 => 165,  269 => 164,  266 => 161,  263 => 160,  257 => 157,  255 => 154,  254 => 153,  253 => 152,  252 => 151,  251 => 150,  249 => 149,  246 => 147,  243 => 146,  240 => 141,  237 => 140,  234 => 138,  231 => 137,  228 => 132,  225 => 131,  218 => 127,  215 => 126,  208 => 120,  205 => 119,  202 => 118,  197 => 98,  193 => 96,  190 => 95,  184 => 92,  181 => 91,  178 => 90,  174 => 87,  165 => 81,  162 => 80,  159 => 79,  155 => 77,  152 => 76,  146 => 74,  144 => 73,  139 => 72,  137 => 69,  136 => 68,  135 => 67,  134 => 65,  132 => 64,  129 => 63,  124 => 215,  120 => 207,  118 => 206,  115 => 205,  113 => 126,  110 => 124,  107 => 118,  105 => 117,  98 => 111,  92 => 109,  89 => 108,  83 => 104,  80 => 103,  77 => 101,  73 => 63,  71 => 62,  69 => 60,  67 => 59,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/vartheme/templates/system/page.html.twig", "C:\\wamp64\\www\\themes\\vartheme\\templates\\system\\page.html.twig");
    }
}
